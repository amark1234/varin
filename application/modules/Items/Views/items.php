<!-- BEGIN PAGE LEVEL STYLES -->
    <div class="row">
        <div class="container portfolio-page margin-bottom-20">
            <div class="filter-v1 margin-top-20">
                <ul class="mix-filter">
                    <li class="filter active" data-filter="all">All</li>
                    <li class="filter" data-filter="category_1">Web</li>
                    <li class="filter" data-filter="category_2">Web Development</li>
                    <li class="filter" data-filter="category_3">Photography</li>
                    <li class="filter" data-filter="category_3 category_1">Wordpress and Logo</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- BEGIN BLOG -->
    <div class="row">
        <!-- BEGIN LEFT SIDEBAR -->
        <div class="col-md-12 col-sm-12 blog-posts margin-bottom-40">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-7 col-sm-7">
                        Topic
                    </div>
                    <div class="col-md-1 col-sm-1">
                        Category
                    </div>
                    <div class="col-md-1 col-sm-1">
                        Users
                    </div>
                    <div class="col-md-1 col-sm-1">
                        Replies
                    </div>
                    <div class="col-md-1 col-sm-1">
                        Views
                    </div>
                    <div class="col-md-1 col-sm-1">
                        Activity
                    </div>
                </div>
            </div>
            <hr class="blog-post-sep">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <h2><a href="blog_item.html"><span class="glyphicon glyphicon-lock blue" style="color: #777;"></span> Corrupti quos dolores etquas</a></h2>
                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui sint blanditiis prae sentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing condimentum eleifend enim a feugiat.</p>
                    <a class="more" href="<?php echo base_url("item_detail")?>">Read more <i class="icon-angle-right"></i></a>
                </div>
                <div class="col-md-1 col-sm-1">
                    <span class="badge-category-bg" style="background-color: #F7941D;"></span>
                    General
                </div>
                <div class="col-md-1 col-sm-1">
                    <img src="<?php echo base_url()?>images/a1.png" class="responsive">
                    <img src="<?php echo base_url()?>images/m1.png" class="responsive">
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12</p>
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>644</p>
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12 June</p>
                </div>
            </div>
            <hr class="blog-post-sep">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <h2><a href="blog_item.html"><i class="fa fa-bullhorn"></i>Corrupti quos dolores etquas</a></h2>
                </div>
                <div class="col-md-1 col-sm-1">
                    <span class="badge-category-bg" style="background-color: #F7941D;"></span>
                    General
                </div>
                <div class="col-md-1 col-sm-1">
                    <img src="<?php echo base_url()?>images/a1.png" class="responsive">
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12</p>
                </div>
                <div class="col-md-1 col-sm-1">
                    <ph4>644</ph4>
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12 June</p>
                </div>
            </div>
            <hr class="blog-post-sep">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <h2><a href="blog_item.html"><i class="fa fa-bullhorn"></i>Corrupti quos dolores etquas</a></h2>
                </div>
                <div class="col-md-1 col-sm-1">
                    <span class="badge-category-bg" style="background-color: #0da3e2;"></span>
                    Food
                </div>
                <div class="col-md-1 col-sm-1">
                    <img src="<?php echo base_url()?>images/a1.png" class="responsive">
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12</p>
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>644</p>
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12 June</p>
                </div>
            </div>
            <hr class="blog-post-sep">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <h2><a href="blog_item.html"><i class="fa fa-bullhorn"></i>Corrupti quos dolores etquas</a></h2>
                </div>
                <div class="col-md-1 col-sm-1">
                    <span class="badge-category-bg" style="background-color: #F7941D;"></span>
                    General
                </div>
                <div class="col-md-1 col-sm-1">
                    <img src="<?php echo base_url()?>images/a1.png" class="responsive">
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12</p>
                </div>
                <div class="col-md-1 col-sm-1">
                    <ph4>644</ph4>
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12 June</p>
                </div>
            </div>
            <hr class="blog-post-sep">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <h2><a href="blog_item.html"><i class="fa fa-bullhorn"></i>Corrupti quos dolores etquas</a></h2>
                </div>
                <div class="col-md-1 col-sm-1">
                    <span class="badge-category-bg" style="background-color: #0da3e2;"></span>
                    Food
                </div>
                <div class="col-md-1 col-sm-1">
                    <img src="<?php echo base_url()?>images/a1.png" class="responsive">
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12</p>
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>644</p>
                </div>
                <div class="col-md-1 col-sm-1">
                    <p>12 June</p>
                </div>
            </div>
        </div>
        <!-- END LEFT SIDEBAR -->
    </div>
<!-- BEGIN PAGE LEVEL PLUGINS -->