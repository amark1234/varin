﻿        <!-- BEGIN BLOG -->
        <div class="row">
            <div class="col-md-9 blog-item">
                <h2><a href="#">Corrupti quos dolores etquas</a></h2>
                <span class="badge-category-bg" style="background-color: #F7941D;"></span>
                General
                <span class="badge-category-bg" style="background-color: #0da3e2;"></span>
                Food
                <i class="icon-tags" style="color: #848484"></i> Metronic, Keenthemes, UI Design
                <hr/>
                <p><img src="<?php echo base_url()?>images/a1.png" class="responsive"> Admin | Leader <span style="float:right" class="text-muted"><i class="icon-calendar"></i> 25/07/2013</span></p>
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui sint blanditiis prae sentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing condimentum eleifend enim a feugiat.</p>
                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                <p>Culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                <!--<ul class="blog-info">
                    <li><i class="icon-user"></i> By admin</li>
                    <li><i class="icon-calendar"></i> 25/07/2013</li>
                    <li><i class="icon-comments"></i> 17</li>
                    <li><i class="icon-tags"></i> Metronic, Keenthemes, UI Design</li>
                </ul>-->
                <div class="media">
                    <h3>Comments</h3>
                    <a href="#" class="pull-left">
                        <img src="images/m1.png" alt="" class="media-object">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">Media heading <span>5 hours ago / <a href="#">Reply</a></span></h4>
                        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                        <hr>
                        <!-- Nested media object -->
                        <div class="media">
                            <a href="#" class="pull-left">
                                <img src="images/a1.png" alt="" class="media-object">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">Media heading <span>17 hours ago / <a href="#">Reply</a></span></h4>
                                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                            </div>
                        </div>
                        <!--end media-->
                        <hr>
                        <div class="media">
                            <a href="#" class="pull-left">
                                <img src="images/m1.png" alt="" class="media-object">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">Media heading <span>2 days ago / <a href="#">Reply</a></span></h4>
                                <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                            </div>
                        </div>
                        <!--end media-->
                    </div>
                </div>
                <!--end media-->
                <div class="media">
                    <a href="#" class="pull-left">
                        <img src="images/m1.png" alt="" class="media-object">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">Media heading <span>July 25,2013 / <a href="#">Reply</a></span></h4>
                        <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                    </div>
                </div>
                <!--end media-->
                <hr>
                <div class="post-comment">
                    <h3>Leave a Comment</h3>
                    <form role="form">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Email <span class="color-red">*</span></label>
                            <input type="text" class="form-control">
                        </div>

                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" rows="8"></textarea>
                        </div>
                        <p><button class="btn btn-default theme-btn" type="submit">Post a Comment</button></p>
                    </form>
                </div>
            </div>
        </div>
        <!-- BEGIN BLOG -->
        <div class="row">
            <h2 class="margin-bottom-30">Suggested Topics</h2>
            <!-- BEGIN LEFT SIDEBAR -->
            <div class="col-md-12 col-sm-12 blog-posts margin-bottom-40">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-7 col-sm-7">
                            Topic
                        </div>
                        <div class="col-md-1 col-sm-1">
                            Category
                        </div>
                        <div class="col-md-1 col-sm-1">
                            Users
                        </div>
                        <div class="col-md-1 col-sm-1">
                            Replies
                        </div>
                        <div class="col-md-1 col-sm-1">
                            Views
                        </div>
                        <div class="col-md-1 col-sm-1">
                            Activity
                        </div>
                    </div>
                </div>
                <hr class="blog-post-sep">
                <div class="row">
                    <div class="col-md-7 col-sm-7">
                        <h4><i class="fa fa-bullhorn"></i>Corrupti quos dolores etquas</h4>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <span class="badge-category-bg" style="background-color: #F7941D;"></span>
                        General
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <img src="<?php echo base_url()?>images/a1.png" class="responsive">
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>12</p>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <ph4>644</ph4>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>12 June</p>
                    </div>
                </div>
                <hr class="blog-post-sep">
                <div class="row">
                    <div class="col-md-7 col-sm-7">
                        <h4><i class="fa fa-bullhorn"></i>Corrupti quos dolores etquas</h4>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <span class="badge-category-bg" style="background-color: #0da3e2;"></span>
                        Food
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <img src="<?php echo base_url()?>images/a1.png" class="responsive">
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>12</p>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>644</p>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>12 June</p>
                    </div>
                </div>
                <hr class="blog-post-sep">
                <div class="row">
                    <div class="col-md-7 col-sm-7">
                        <h4><i class="fa fa-bullhorn"></i>Corrupti quos dolores etquas</h4>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <span class="badge-category-bg" style="background-color: #F7941D;"></span>
                        General
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <img src="<?php echo base_url()?>images/a1.png" class="responsive">
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>12</p>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <ph4>644</ph4>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>12 June</p>
                    </div>
                </div>
                <hr class="blog-post-sep">
                <div class="row">
                    <div class="col-md-7 col-sm-7">
                        <h4><i class="fa fa-bullhorn"></i>Corrupti quos dolores etquas</h4>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <span class="badge-category-bg" style="background-color: #0da3e2;"></span>
                        Food
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <img src="<?php echo base_url()?>images/a1.png" class="responsive">
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>12</p>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>644</p>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <p>12 June</p>
                    </div>
                </div>
            </div>
            <!-- END LEFT SIDEBAR -->
        </div>
        <div>
            <h2 class="margin-bottom-30">Want to read more? Browse other topics in Mobile App & Web Client or view latest topics.</h2>
        </div>