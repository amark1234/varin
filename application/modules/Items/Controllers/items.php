<?php
/*
Author: Daniel Gutierrez
Date: 9/18/12
Version: 1.0
*/

class Items extends MY_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('items_model');
    }

    function index(){
        $data['main_content'] = 'items';
        $this->load->view('page_front', $data);
    }

    function item_detail(){
        $data['main_content'] = 'items_detail';
        $this->load->view('page_front', $data);
    }
}

?>