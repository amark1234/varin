<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sign in</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le styles -->
	<link href="<?php echo base_url(); ?>theme/css/bootstrap.css" rel="stylesheet">
	<style type="text/css">
		body {
			padding-top: 60px;
			padding-bottom: 40px;
		}
	</style>
	<link href="<?php echo base_url(); ?>theme/css/bootstrap-responsive.css" rel="stylesheet">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-57-precomposed.png">

	<!-- Javascript -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>theme/js/bootstrap.js"></script>
</head>

<body>
<div class="row">
	<div class="span6 offset3">
		<h1>Sign in</h1>

		<?php if(@$error): ?>
			<div class="alert">
				<button type="button" class="close" data-dismiss="alert">?</button>
				<?php echo $error; ?>
			</div>
		<?php endif; ?>
		<?php if(@$success): ?>
			<div class="alert">
				<button type="button" class="close" data-dismiss="alert">?</button>
				<?php echo $success; ?>
			</div>
		<?php endif; ?>

		<div class="well">
			<form class="form-horizontal" method="post" action="<?php echo base_url('users/signin')?>">
				<div class="control-group">
					<label class="control-label" for="inputEmail">Email</label>
					<div class="controls">
						<input type="text" id="inputEmail" placeholder="Email" name="user_email" value="">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPassword">Password</label>
					<div class="controls">
						<input type="password" id="inputPassword" placeholder="Password" name="password" value="">
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<label class="checkbox">
							<input type="checkbox" name="remember"> Remember me
						</label>
						<button type="submit" class="btn">Sign in</button>
						<button type="button" class="btn" onclick="window.location.href='<?php echo base_url('users/signup');?>'">Sign up</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
</body>
</html>