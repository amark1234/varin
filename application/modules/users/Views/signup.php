<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sign up</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Le styles -->
	<link href="<?php echo base_url(); ?>theme/css/bootstrap.css" rel="stylesheet">
	<style type="text/css">
		body {
			padding-top: 60px;
			padding-bottom: 40px;
		}
	</style>
	<link href="<?php echo base_url(); ?>theme/css/bootstrap-responsive.css" rel="stylesheet">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-57-precomposed.png">

	<!-- Javascript -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>theme/js/bootstrap.js"></script>
</head>

<body>
<div class="row">
	<div class="span6 offset3">
		<h1>Sign up</h1>

		<?php if(@$error): ?>
			<div class="alert">
				<button type="button" class="close" data-dismiss="alert">?</button>
				<?php echo $error; ?>
			</div>
		<?php endif; ?>


		<div class="well">

			<form class="form-horizontal" method="post" action="">
				<div class="control-group">
					<label class="control-label" for="inputFullName">Full Name</label>
					<div class="controls">
						<input type="text" id="inputFullName" value="<?php echo set_value('fullname'); ?>" name="fullname">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">Email</label>
					<div class="controls">
						<input type="text" id="inputEmail" value="<?php echo set_value('email'); ?>" name="email">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputEmail">Username</label>
					<div class="controls">
						<input type="text" id="inputEmail" value="<?php echo set_value('username'); ?>" name="username">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="inputPassword">Password</label>
					<div class="controls">
						<input type="password" id="inputPassword" name="password">
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<button type="button" class="btn" onclick="window.location.href='<?php echo base_url('users/signin');?>'">< Back</button>
						<button type="submit" class="btn">Sign up</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>