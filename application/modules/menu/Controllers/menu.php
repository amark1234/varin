<?php
/*
Author: Daniel Gutierrez
Date: 9/18/12
Version: 1.0
*/

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MY_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->module("users");

		if(!$this->_is_logged_in()){
			$this->load->module('users');
			$this->users->signin();
		}
	}
	
	function index(){
		$data['current'] = $this->uri->segment(1);
		$data['items'] = $this->menu_model->read();
		$data['query'] = $this->menu_model->get_modules_config();
		//Admin links
		if($this->users->_is_admin()){
			$data['items'][] = $this->menu_model->menu_admin();
		}
		
		$data['currentuser'] = @$this->users->userdata();

		$this->load->view("menu", $data);
	}

	function _is_logged_in(){
		if($this->session->userdata('logged_in')){
			return true;
		}else{
			return false;
		}
	}
	
	//Limit access
	function _remap(){
		show_404();
	}
		
}

?>