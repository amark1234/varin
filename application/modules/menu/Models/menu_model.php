<?php
/*
Author: Daniel Gutierrez
Date: 9/18/12
Version: 1.0
*/

class Menu_model extends CI_Model{
	var $table_config = "modules_config";
	var $table_setting = "modules_setting";
	
	function create(){
		
	}
	
	function read(){
		//Just a prototype
		$menu = array();
		
		$menu[0] = new stdClass();
		$menu[0]->url = "";
		$menu[0]->name = "Home";
		
		$menu[1] = new stdClass();
		$menu[1]->url = "users";
		$menu[1]->name = "Users";
		
		return $menu;
	}
	
	function menu_admin(){
		//Just a prototype

		$menu = new stdClass();
		$menu->url = "admin";
		$menu->name = "Admin";

		return $menu;
	}
	
	function update(){
		
	}
	
	function delete(){
		
	}

	function get_modules_config(){
		$query = $this->db->query("SELECT * FROM $this->table_config WHERE $this->table_config.mod_active = '1'");
		return $query->result();
	}

	function get_modules_setting($id){
		$query = $this->db->query("SELECT * FROM $this->table_setting WHERE $this->table_setting.mod_id = $id");
		return $query->row();
	}
		
}

?>