<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class First extends MY_Controller {
    function __construct(){
        parent::__construct();
        modules::run("users");
    }

    public function index()
    {
        $this->load->view('welcome_message');
    }

    function signup(){
        if($_POST){
                $data['user_login']		= $this->input->post('fullname',true);
                $data['user_pass']		= md5($this->input->post('password',true));
                $data['user_nicename']	= $this->input->post('username',true);
                $data['user_email']		= $this->input->post('email',true);
                $data['activation_key']	= md5(rand(0,1000).'uniquefrasehere');
                $create = $this->user_model->create($data);

                if($create){
                    //Send validation mail
                    $this->load->library('email');

                    $this->email->from('noreply@yoursite.com', 'Site Name');
                    $this->email->to($data['user_email']);

                    $this->email->subject('Confirmation');
                    $this->email->message("Confirm your subscription <a href=''>Confirmar</a>".$data['activation_key']);
                    $this->email->send();

                    redirect("items");
                }else{
                    error_log("Un usuario no se pudo registrar");
                }
            }

        return;
        redirect("items");
    }
}