<!-- Sign up -->
<div class="modal fade" id="signup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Create New Account</h4>
                <?php if(@$error): ?>
                    <div class="alert">
                        <button type="button" class="close" data-dismiss="alert">?</button>
                        <?php echo $error; ?>
                    </div>
                <?php endif; ?>
            </div>
            <form action="first/signup" method="post" name="signup_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-md-1"></label>
                            <label class="control-label col-md-2">Full name :</label>
                            <div class="col-md-8">
                                <input type="text" name="fullname" placeholder="Full name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group margin-top-10">
                            <label class="control-label col-md-1"></label>
                            <label class="control-label col-md-2">Email :</label>
                            <div class="col-md-8">
                                <input type="text" name="email" placeholder="Email" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="form-group">
                            <label class="control-label col-md-1"></label>
                            <label class="control-label col-md-2">Username :</label>
                            <div class="col-md-8">
                                <input type="text" name="username" placeholder="Username" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="form-group">
                            <label class="control-label col-md-1"></label>
                            <label class="control-label col-md-2">Password :</label>
                            <div class="col-md-8">
                                <input type="text" name="password" placeholder="Password" class="form-control">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn blue">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Login -->

<div class="modal fade" id="login" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">
            <form action="#" class="form-horizontal" method="post" name="form_sample_1" id="form_sample_1">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Login</h4>
                </div>

                <div class="modal-body">
                    <div class="row margin-top-10">
                        <div class="form-group">
                            <label class="control-label col-md-1"></label>
                            <label class="control-label col-md-2">Username :</label>
                            <div class="col-md-8">
                                <input type="text" name="username" placeholder="Username" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="form-group">
                            <label class="control-label col-md-1"></label>
                            <label class="control-label col-md-2">Password :</label>
                            <div class="col-md-8">
                                <input type="text" name="password" placeholder="Password" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn blue">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
