<?php $this->load->view('includes/first_modal'); ?>
<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS(REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>assets_f/plugins/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets_f/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_f/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets_f/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/plugins/hover-dropdown.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/plugins/back-to-top.js"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL JAVASCRIPTS(REQUIRED ONLY FOR CURRENT PAGE) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets_f/plugins/bxslider/jquery.bxslider.min.js"></script>
<script src="<?php echo base_url(); ?>assets_f/plugins/jquery.mixitup.min.js"></script>
<script src="<?php echo base_url(); ?>assets_f/scripts/portfolio.js"></script>
<script src="<?php echo base_url(); ?>assets_f/scripts/app.js"></script>
<script src="<?php echo base_url(); ?>assets_f/scripts/index.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function() {
    //FormValidation.init();
    App.init();
    App.initBxSlider();
    Index.initRevolutionSlider();
    Portfolio.init();

  });
</script>
<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
