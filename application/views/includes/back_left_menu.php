<!-- BEGIN SIDEBAR -->
<div class="page-sidebar navbar-collapse collapse">
<!-- BEGIN SIDEBAR MENU -->
<ul class="page-sidebar-menu">
<li>
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <div class="sidebar-toggler hidden-phone"></div>
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
</li>
<li>
    <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
    <form class="sidebar-search" action="extra_search.html" method="POST">
        <div class="form-container">
            <div class="input-box">
                <a href="javascript:;" class="remove"></a>
                <input type="text" placeholder="Search..."/>
                <input type="button" class="submit" value=" "/>
            </div>
        </div>
    </form>
    <!-- END RESPONSIVE QUICK SEARCH FORM -->
</li>
<li class="start active ">
    <a href="index.html">
        <i class="icon-home"></i>
        <span class="title">Dashboard</span>
        <span class="selected"></span>
    </a>
</li>
    <?php
        foreach($query as $k => $v):
    ?>
            <li class="">
                <a href="javascript:;">
                    <i class="<?php echo $v->mod_icon?>"></i>
                    <span class="title"><?php echo $v->mod_name?></span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <?php
                        $query_setting = $this->home_model->get_modules_setting($v->id);
                        if($query_setting->has_list == '1'):
                    ?>
                        <li >
                            <a href="<?php echo base_url()?>">
                                List <?php echo $v->mod_name?>
                            </a>
                        </li>
                    <?php
                        endif;
                        if($query_setting->has_new == '1'):
                    ?>
                        <li >
                            <a href="<?php echo base_url()?>">
                                New <?php echo $v->mod_name?>
                            </a>
                        </li>
                    <?php
                        endif;
                    ?>

                </ul>
            </li>
    <?php
        endforeach;
    ?>

</ul>
<!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->

