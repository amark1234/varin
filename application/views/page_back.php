﻿<?php
if(!$this->session->userdata('logged_in')){
    $this->load->view('signin');
}else{
    ?>
    <?php $this->load->view('includes/back_header'); ?>
    <?php //$this->load->view('includes/page_left_menu'); ?>
    <?php echo modules::run("menu"); ?>
    <!-- BEGIN CONTAINER -->
    <div class="page-content">
        <?php $this->load->view($main_content); ?>
    </div>
    <!-- END CONTAINER -->
    <?php $this->load->view('includes/back_footer'); ?>
    <?php
}
?>
